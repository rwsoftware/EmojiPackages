set packages=packages
rem goto repo

call copyresource ru.rwsoftware.eyecu.emoji.vkontakte.16 vKontakte\png\16
call copyresource ru.rwsoftware.eyecu.emoji.vkontakte.32 vKontakte\png\32
call copyresource ru.rwsoftware.eyecu.emoji.emojione.40 "Emoji One\png\40"
call copyresource ru.rwsoftware.eyecu.emoji.emojione.64 "Emoji One\png\64"
call copyresource ru.rwsoftware.eyecu.emoji.emojione.72 "Emoji One\png\72"
call copyresource ru.rwsoftware.eyecu.emoji.emojione.128 "Emoji One\png\128"

call copyresource ru.rwsoftware.eyecu.emoji.apple.16 "Apple\png\16"
call copyresource ru.rwsoftware.eyecu.emoji.apple.32 "Apple\png\32"
call copyresource ru.rwsoftware.eyecu.emoji.apple.40 "Apple\png\40"
call copyresource ru.rwsoftware.eyecu.emoji.apple.48 "Apple\png\48"
call copyresource ru.rwsoftware.eyecu.emoji.apple.64 "Apple\png\64"
call copyresource ru.rwsoftware.eyecu.emoji.apple.72 "Apple\png\72"

call copyresource ru.rwsoftware.eyecu.emoji.google.16 "Google\png\16"
call copyresource ru.rwsoftware.eyecu.emoji.google.32 "Google\png\32"
call copyresource ru.rwsoftware.eyecu.emoji.google.40 "Google\png\40"
call copyresource ru.rwsoftware.eyecu.emoji.google.48 "Google\png\48"
call copyresource ru.rwsoftware.eyecu.emoji.google.64 "Google\png\64"
call copyresource ru.rwsoftware.eyecu.emoji.google.72 "Google\png\72"

call copyresource ru.rwsoftware.eyecu.emoji.twitter.16 "Twitter\png\16"
call copyresource ru.rwsoftware.eyecu.emoji.twitter.32 "Twitter\png\32"
call copyresource ru.rwsoftware.eyecu.emoji.twitter.40 "Twitter\png\40"
call copyresource ru.rwsoftware.eyecu.emoji.twitter.48 "Twitter\png\48"
call copyresource ru.rwsoftware.eyecu.emoji.twitter.64 "Twitter\png\64"
call copyresource ru.rwsoftware.eyecu.emoji.twitter.72 "Twitter\png\72"

call copyresource ru.rwsoftware.eyecu.emoji.samsung.16 "Samsung\png\16"
call copyresource ru.rwsoftware.eyecu.emoji.samsung.32 "Samsung\png\32"
call copyresource ru.rwsoftware.eyecu.emoji.samsung.40 "Samsung\png\40"
call copyresource ru.rwsoftware.eyecu.emoji.samsung.48 "Samsung\png\48"
call copyresource ru.rwsoftware.eyecu.emoji.samsung.64 "Samsung\png\64"
call copyresource ru.rwsoftware.eyecu.emoji.samsung.72 "Samsung\png\72"

call copyresource ru.rwsoftware.eyecu.emoji.chart.16 "BW Chart\png\16"
call copyresource ru.rwsoftware.eyecu.emoji.chart.32 "BW Chart\png\32"
call copyresource ru.rwsoftware.eyecu.emoji.chart.40 "BW Chart\png\40"
call copyresource ru.rwsoftware.eyecu.emoji.chart.48 "BW Chart\png\48"
call copyresource ru.rwsoftware.eyecu.emoji.chart.64 "BW Chart\png\64"
call copyresource ru.rwsoftware.eyecu.emoji.chart.72 "BW Chart\png\72"

call copyresource ru.rwsoftware.eyecu.emoji.emojionebw.16 "BW EmojiOne\png\16"
call copyresource ru.rwsoftware.eyecu.emoji.emojionebw.32 "BW EmojiOne\png\32"
call copyresource ru.rwsoftware.eyecu.emoji.emojionebw.40 "BW EmojiOne\png\40"
call copyresource ru.rwsoftware.eyecu.emoji.emojionebw.48 "BW EmojiOne\png\48"
call copyresource ru.rwsoftware.eyecu.emoji.emojionebw.64 "BW EmojiOne\png\64"

call copyresource ru.rwsoftware.eyecu.emoji.fb.16 "Facebook\png\16"
call copyresource ru.rwsoftware.eyecu.emoji.fb.32 "Facebook\png\32"
call copyresource ru.rwsoftware.eyecu.emoji.fb.40 "Facebook\png\40"
call copyresource ru.rwsoftware.eyecu.emoji.fb.48 "Facebook\png\48"
call copyresource ru.rwsoftware.eyecu.emoji.fb.64 "Facebook\png\64"
call copyresource ru.rwsoftware.eyecu.emoji.fb.72 "Facebook\png\72"

call copyresource ru.rwsoftware.eyecu.emoji.fbm.16 "Facebook Mobile\png\16"
call copyresource ru.rwsoftware.eyecu.emoji.fbm.32 "Facebook Mobile\png\32"
call copyresource ru.rwsoftware.eyecu.emoji.fbm.40 "Facebook Mobile\png\40"
call copyresource ru.rwsoftware.eyecu.emoji.fbm.48 "Facebook Mobile\png\48"
call copyresource ru.rwsoftware.eyecu.emoji.fbm.64 "Facebook Mobile\png\64"
call copyresource ru.rwsoftware.eyecu.emoji.fbm.72 "Facebook Mobile\png\72"

call copyresource ru.rwsoftware.eyecu.emoji.windows.16 "Windows\png\16"
call copyresource ru.rwsoftware.eyecu.emoji.windows.32 "Windows\png\32"
call copyresource ru.rwsoftware.eyecu.emoji.windows.40 "Windows\png\40"
call copyresource ru.rwsoftware.eyecu.emoji.windows.48 "Windows\png\48"
call copyresource ru.rwsoftware.eyecu.emoji.windows.64 "Windows\png\64"
call copyresource ru.rwsoftware.eyecu.emoji.windows.72 "Windows\png\72"

pause
:repo
repogen.exe -p %packages% repository
pause
:end